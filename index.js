const puppeteer = require('puppeteer');
const axios     = require('axios');
const express   = require('express');
const cors      = require('cors');

const GENIUS_TOKEN = 'KI9C3Rje8b1eOx2Rg01b3h-q9vFvzh4JYDvgg0BHgwuxqXCzYWBg79fJphA_sdAt';


async function main() {
    const app = express();
    app.use(cors());
    const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });

    const axiosClient = axios.create({
        baseURL: 'https://api.genius.com/search',
        timeout: 10000,
        headers: {
            'Authorization': `Bearer ${GENIUS_TOKEN}`
        }
    });

    const lyricsCache = new Map;
    function cacheShrink() {
        if(lyricsCache.size > 256) {
            const nrOldKeys = lyricsCache.size - 256;
            const keys = Array.from(lyricsCache.keys()).slice(0, nrOldKeys);
            keys.forEach(k => lyricsCache.delete(k));
        }
    }

    app.get('/search', function(req, res) {
        const query = req.query.q ?? '';
        axiosClient.get(`?q=${encodeURI(query)}`)
        .then(response => {
            res.send({
                data: response.data.response.hits
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        });
    });

    app.get('/lyrics', async function(req, res) {
        const url = req.query.url;
        if(lyricsCache.has(url)) {
            const data = lyricsCache.get(url);
            lyricsCache.delete(url);
            lyricsCache.set(url, data);
            res.send({ data });
            return;
        }
        if(!url) {
            res.status(400).send({
                message: "Invalid param 'url'"
            });
            return;
        }
        const page = await browser.newPage();
        await page.goto(url, { waitUntil: 'networkidle2' });
        const lyrics = await page.evaluate(_ => {
            return new Promise(r => {
                const intervalID = setInterval(() => {
                    function getElement() {
                        return document.querySelector('div[class*="Lyrics__Container"],section[text-selection-changed*="lyrics_ctrl.update_selection_range"]')
                    }
                    if(getElement()) {
                        clearInterval(intervalID);
                        r(getElement().innerText);
                    }
                }, 300);
            });
        });
        await page.close();
        lyricsCache.set(url, lyrics);
        cacheShrink();
        res.send({
            'data': lyrics
        });
    });

    app.listen(3002);
}


main()

